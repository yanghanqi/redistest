package com.yhq.redistest.controller;
import	java.util.HashMap;

import com.yhq.redistest.util.RedisCommonUtil;
//import io.lettuce.core.RedisClient;
//import io.lettuce.core.api.StatefulRedisConnection;
//import io.lettuce.core.api.async.RedisAsyncCommands;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.concurrent.ExecutionException;

@Controller
@RequestMapping("/redis")
public class MainController {

    @Autowired(required = false)
    RedisCommonUtil<String> redisCommonUtil;
    @Autowired
    @Qualifier("jedisTemplate")
    RedisTemplate<String, Object> redisTemplate;

    @RequestMapping("/testSpring")
    @ResponseBody
    public String testSpring(){

        String key = "key1";
        String value = "key1_value";
//        long start = System.currentTimeMillis();
//        redisCommonUtil.setCache(key, value);
//        System.out.println(System.currentTimeMillis()-start);
        long start2 = System.currentTimeMillis();
        for(int i=0; i<10000; i++) {
            redisCommonUtil.getCache(key);
        }
        System.out.println((System.currentTimeMillis()-start2));

        return "OK";
    }

    @RequestMapping("/testJedis")
    @ResponseBody
    public String testJedis(){

        Jedis jedis = new Jedis("192.168.1.27", 6379);

        String key = "key1";
        String value = "key1_value";
//        long start = System.currentTimeMillis();
//        jedis.set(key, value);
//        System.out.println(System.currentTimeMillis()-start);
        long start2 = System.currentTimeMillis();
        for(int i=0; i<10000; i++) {
            jedis.get(key);
        }
        System.out.println(System.currentTimeMillis()-start2);

        return "OK";
    }

    @RequestMapping("/testLet")
    @ResponseBody
    public String testLet() throws ExecutionException, InterruptedException {

////        RedisClient client = RedisClient.create("redis://192.168.1.27:6379");
////        StatefulRedisConnection<String, String> connection = client.connect();
////        RedisAsyncCommands<String, String> commands = connection.async();
//        String key = "key1";
//        String value = "key1_value";
//        long start2 = System.currentTimeMillis();
//        for(int i=0; i<10000; i++) {
//            commands.get(key).get();
//        }
//        System.out.println(System.currentTimeMillis()-start2);

        return "OK";
    }

    @RequestMapping("/testSpringJedis")
    @ResponseBody
    public String testSpringJedis() throws ExecutionException, InterruptedException {

        String key = "key1";
        String value = "key1_value";
        long start2 = System.currentTimeMillis();
        for(int i=0; i<10000; i++) {
            redisTemplate.opsForValue().get(key);
        }
        System.out.println(System.currentTimeMillis()-start2);

        return "OK";
    }

    @RequestMapping("/testMap")
    @ResponseBody
    public String testMap() throws ExecutionException, InterruptedException {

        Map map = new HashMap();
        String key = "key1";
        String value = "key1_value";

        map.put(key, value);
        long start2 = System.currentTimeMillis();
        for(int i=0; i<10000; i++) {
            map.get(key);
        }
        System.out.println(System.currentTimeMillis()-start2);

        return "OK";
    }
}
